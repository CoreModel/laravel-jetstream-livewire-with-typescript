# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.2] - 2020-10-03
### Migrated
- Tailwind

## [0.0.1] - 2020-10-03
### Added
- Changelog
- Contributing
- License

## [0.0.0] - 2020-10-03
### Added
- Initial application based on Laravel Jetstream Livewire with TypeScript

[Unreleased]: https://gitlab.com/CoreModel/laravel-jetstream-livewire-with-typescript/-/compare/v0.0.2...master
[0.0.2]: https://gitlab.com/CoreModel/laravel-jetstream-livewire-with-typescript/-/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/CoreModel/laravel-jetstream-livewire-with-typescript/-/compare/v0.0.0...v0.0.1
[0.0.0]: https://gitlab.com/CoreModel/laravel-jetstream-livewire-with-typescript/-/tree/v0.0.0